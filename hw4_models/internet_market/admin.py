from django.contrib import admin
from internet_market.models import Product, Client, ProductSet

admin.site.register(Client)
admin.site.register(Product)
admin.site.register(ProductSet)
