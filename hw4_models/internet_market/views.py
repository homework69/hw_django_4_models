from django.shortcuts import render
from django.http import HttpResponse
from internet_market.models import Product, Client
from datetime import datetime
from uuid import uuid4


def create_product(request):
    for n in range(5):
        good = Product()
        good.description = "Line 1 of description" \
                           "\nline 2 of description" \
                           "\nline 3 of description"
        good.label = "Product " + str(n+1)
        good.count = n
        good.manufactured_date = datetime(2022, 9-n, 1)
        used_weight = n % 2
        good.used_weight = used_weight
        good.weight = 1 if used_weight else 0
        good.price = 1 + n ** 2
        good.web_ref = "example-product.com"
        good.product_uuid = uuid4()
        good.save()

    return HttpResponse("Created!")


def create_client(request):
    client_list = []
    for n in range(5):
        client_properties = {
            'client_name': f'client{n+1}',
            'client_email': f'client{n+1}@gmail.com',
            'client_ip': f'192.0.0.{n+10}',
            'personal_discount': 0.01 * (n+1)
        }
        client_list.append(client_properties)
    for elem in client_list:
        Client.objects.create(**elem)
    return HttpResponse("Created!")


def get_product(request, id_value):
    product_obj = Product.objects.get(id=id_value)
    product_repr = product_obj.label + " (" + str(product_obj.price) + ")"
    return HttpResponse(product_repr)

