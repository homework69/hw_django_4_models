from django.db import models
from datetime import timedelta

from django.contrib.auth.models import User


class Product(models.Model):
    label = models.CharField(max_length=100, unique=True, blank=True, null=True)
    count = models.IntegerField(default=0, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    manufactured_date = models.DateField(blank=True, null=True)
    delivered_at = models.DateTimeField(auto_now_add=True, null=True)
    expiration_term = models.DurationField(blank=True, default=timedelta(days=365), null=True)
    used_weight = models.BooleanField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    web_ref = models.URLField(blank=True, name='Internet ref', null=True)
    product_uuid = models.UUIDField(editable=False, null=True)


class ProductSet(models.Model):
    label = models.CharField(max_length=100, unique=True, blank=True, null=True)
    photo = models.ImageField(blank=True, null=True, verbose_name='Foto of set', upload_to='images/%Y/%m/%d/')
    instruction_file = models.FileField(blank=True, null=True, verbose_name='Instruction', upload_to='uploads/%Y/%m/%d/')
    accessories = models.ManyToManyField(Product, verbose_name='The set consist of')
    description = models.TextField(blank=True, null=True, help_text="Add description if it's needed")


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    client_name = models.CharField(max_length=100, unique=True, null=True)
    client_email = models.EmailField(blank=True, null=True)
    client_ip = models.GenericIPAddressField(protocol="IPv4", null=True)
    personal_discount = models.DecimalField(blank=True, max_digits=4, decimal_places=2, null=True)





