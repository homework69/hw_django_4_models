from django.urls import path
from internet_market import views

urlpatterns = [
    path('create-product/', views.create_product, name='create_product'),
    path('create-client/', views.create_client, name='create_client'),
    path('get_product/<int:id_value>/', views.get_product, name='get_product')
]