from django.apps import AppConfig


class InternetMarketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'internet_market'
